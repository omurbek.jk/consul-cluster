consul-cluster
==============

This repo is designed to work with the [CloudCoreo](http://www.cloudcoreo.com) engine. 


## Description
Highly Available, self-healing consul cluster configuration and setup

Consul is an awesome product from the guys over at Hashicorp. It is a key/value store and service discovery mechanism (like zookeeper) as well as health-checker and DNS provider. It's also ready to support multiple datacenters out of the box.

This [CloudCoreo](http://www.cloudcoreo.com) stack provides a production-ready Consul Server Cluster that is self-healing and highly available. If defaults are left as is, your environment will launch a cluster of 3 t2.small instances.

## How does it work?
You must provide a route53 dns zone for us to add an entry to. This will be a CNAME pointing to an internal ELB. The internal ELB will provide healthchecks for the consul servers and automatically replace failed nodes. The url will be dictated by the variable: `CONSUL&#95;NAME` which defaults to "consul".

i.e. if your `CONSUL&#95;NAME` is left as the default "consul", your consul cluster UI will be available at http://consul.<dns&#95;name>

This is a private ELB so you can only access via VPN or bastion depending on how your network is set up.

When a failure even takes place, the Autoscaling group will replace the failed node. The addition of a new node triggers a clean up process approximately 60 seconds after boot is complete.


## Hierarchy
![composite inheritance hierarchy](https://raw.githubusercontent.com/CloudCoreo/consul-cluster/master/images/hierarchy.png "composite inheritance hierarchy")



## Required variables with no default

### `VPC_NAME`:
  * description: the vpc to launch in

### `PRIVATE_SUBNET_NAME`:
  * description: name of the cloudcoreo managed private subnet

### `DNS_ZONE`:
  * description: the dns zone we are going to add records to


## Required variables with default

### `CONSUL_NAME`:
  * description: name of the consul server
  * default: consul


### `CONSUL_INGRESS_CIDRS`:
  * description: cidrs that can access the consul server
  * default: 10.11.0.0/16

### `CONSUL_SIZE`:
  * description: the image size to launch
  * default: t2.small


### `CONSUL_GROUP_SIZE_MIN`:
  * description: the minimum number of instances to launch
  * default: 3

### `CONSUL_GROUP_SIZE_MAX`:
  * description: the maxmium number of instances to launch
  * default: 5

### `CONSUL_SERVER_SG_NAME`:
  * description: the consul security group name
  * default: consul



## Optional variables with default

### `CONSUL_KEY`:
  * description: the ssh key to associate with the instance(s) - blank will disable ssh
  * default: 


## Optional variables with no default

### `CONSUL_AMI`:
  * description: the ami to launch for consul - default is Amazon Linux AMI 2015.03 (HVM), SSD Volume Type

## Tags
1. Service Discovery
1. DNS
1. Monitoring
1. Hashicorp
1. High Availability

## Categories
1. Servers



## Diagram
![diagram](https://raw.githubusercontent.com/CloudCoreo/consul-cluster/master/images/diagram.png "diagram")


## Icon
![icon](https://raw.githubusercontent.com/CloudCoreo/consul-cluster/master/images/icon.png "icon")

