Highly Available, self-healing consul cluster configuration and setup

Consul is an awesome product from the guys over at Hashicorp. It is a key/value store and service discovery mechanism (like zookeeper) as well as health-checker and DNS provider. It's also ready to support multiple datacenters out of the box.

This [CloudCoreo](http://www.cloudcoreo.com) stack provides a production-ready Consul Server Cluster that is self-healing and highly available. If defaults are left as is, your environment will launch a cluster of 3 t2.small instances.

## How does it work?
You must provide a route53 dns zone for us to add an entry to. This will be a CNAME pointing to an internal ELB. The internal ELB will provide healthchecks for the consul servers and automatically replace failed nodes. The url will be dictated by the variable: `CONSUL&#95;NAME` which defaults to "consul".

i.e. if your `CONSUL&#95;NAME` is left as the default "consul", your consul cluster UI will be available at http://consul.<dns&#95;name>

This is a private ELB so you can only access via VPN or bastion depending on how your network is set up.

When a failure even takes place, the Autoscaling group will replace the failed node. The addition of a new node triggers a clean up process approximately 60 seconds after boot is complete.
